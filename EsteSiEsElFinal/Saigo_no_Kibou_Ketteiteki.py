# Bibliotecas
import time, random, math, sys
from Bio.PDB import *


# Función que ajusta las coordenadas
def plega(d, l):
    imagen = math.floor(d / l)
    d -= imagen * l
    if d >= 0.5 * l:
        d -= l
    return d

#Función para escribir el archivo de salida con los frames calculados
def escribe_frame(salida, x, y, z):
    name = 'NE'
    resname = 'NE'
    chain = 'A'
    segname = 'A'

    # Se divide en dos, para ajustar los limites de la caja
    lx2 = lx / 2
    ly2 = ly / 2
    lz2 = lz / 2
    # once espacios
    salida.write('CRYST1 %8.3f %8.3f %8.3f  %8.3f %8.3f %8.3f P 1           1\n' % (lx2, ly2, lz2, -lx2, -ly2, -lz2))
    for i in range(num_atoms):
        resid = i + 1
        xp = plega(x[i], lx)
        yp = plega(y[i], ly)
        zp = plega(z[i], lz)
        salida.write('ATOM  %5d  %-3s %-4s%s%4d    %8.3f%8.3f%8.3f %5.2f %5.2f      %-4s\n' % (
            i + 1, name, resname, chain, resid, xp, yp, zp, 1.0, 1.0, segname))
    salida.write('END\n')

#Función para saber las particulas cercanas
def get_vecinos(x0, y0, z0):
    nx = int(math.floor(lx / cutoff))
    ny = int(math.floor(ly / cutoff))
    nz = int(math.floor(lz / cutoff))
    tamano_celda_x = lx / nx
    tamano_celda_y = ly / ny
    tamano_celda_z = lz / nz

    ix = int(math.floor(x0 / tamano_celda_x))
    iy = int(math.floor(y0 / tamano_celda_y))
    iz = int(math.floor(z0 / tamano_celda_z))
    ret = []
    for jx in range(ix - 1, ix + 2):
        for jy in range(iy - 1, iy + 2):
            for jz in range(iz - 1, iz + 2):
                jx1 = int(plega(jx, nx))
                jy1 = int(plega(jy, ny))
                jz1 = int(plega(jz, nz))
                c1 = jx1 + nx * jy1 + nx * ny * jz1
                ret.append(c1)
    return ret

#Función que permite la periodicidad de los átomos y se relaciona con get_vecinos para las interacciones
def descomposicion(x, y, z):
    celda = {}
    for i in range(num_total):
        vecinos = get_vecinos(x[i], y[i], z[i])
        c = vecinos[4]
        if c in celda.keys():
            celda[c].append(i)
        else:
            celda[c] = [i]
    return celda


#función para el calcular la función de verlet    
def calculo_Verlet (i,x,y,z,x_prev,y_prev,z_prev,fx,fy,fz,masa,dt):
    x_prox[i] = 0.0
    y_prox[i] = 0.0
    z_prox[i] = 0.0 
    x_prox[i] = 2*x[i] - x_prev[i] + fx[i]/masa*dt*dt
    y_prox[i] = 2*y[i] - y_prev[i] + fy[i]/masa*dt*dt
    z_prox[i] = 2*z[i] - z_prev[i] + fz[i]/masa*dt*dt
    return x_prox, y_prox, z_prox

#función para el calculo de la energía cinetica
def calculo_energia_cinetica(i,x_prox,x_prev,y_prox,y_prev,z_prox,z_prev,dt):
    velx = 0.0
    vely = 0.0
    velz = 0.0
    velx = 0.5*(x_prox[i]-x_prev[i])/dt
    vely = 0.5*(y_prox[i]-y_prev[i])/dt
    velz = 0.5*(z_prox[i]-z_prev[i])/dt
    return velx, vely, velz


# función que permite calcular el valor más lejano para generar la caja
def coor_caja(a, b):
    val = a - b
    val_return = 0.0
    if (val < 0.0):
        val_return = b
    if (val > 0.0):
        val_return = a
    if (val == 0.0):
        val_return = a
    return val_return


# Variables por consola
pdb_file = sys.argv[1] #archivo de entrada
pdb_output_file = sys.argv[2]#archivo de salida
num_atoms = int(sys.argv[3]) #número de átomos de neón que se generarán
pasos = int(sys.argv[4])#número de pasos para la dinámica
frames = int(sys.argv[5])#cada cuanto generar un frame

# Parametros
timestep = 2.0;  # en femtosegundos
K = 20.0;  # en kcal/mol/angstrom^2
masa = 20.017970;  # en daltons
lj_e = 0.08545;  # en kcal/mol
lj_r = 3.06;  # en angstroms
R = 0.5 * lj_r;  # en angstroms
archivo_salida = pdb_output_file
cutoff = 5.0;  # en angstroms

lj_r6 = lj_r ** 6;
lj_r12 = lj_r ** 12

factor_tiempo = 48.88821
dt = timestep / factor_tiempo
escala = 10.0

x = [];
x_prev = [];
x_prox = [];
fx = []
y = [];
y_prev = [];
y_prox = [];
fy = []
z = [];
z_prev = [];
z_prox = [];
fz = []

# Para leer pdb
parser = PDBParser()
structure = parser.get_structure('PHA-L', pdb_file)

# Arrays para almacenar coordenadas de los atomos del PDB
x_coor_prot = []
y_coor_prot = []
z_coor_prot = []

# Para obtener datos del pdb
for atom in structure.get_atoms():
    datos_atoms = atom.get_vector()
    x_coor_prot.append(datos_atoms[0])
    y_coor_prot.append(datos_atoms[1])
    z_coor_prot.append(datos_atoms[2])

# obtener el valor máximo de las coordenadas
x_max = max(x_coor_prot)
y_max = max(y_coor_prot)
z_max = max(z_coor_prot)
# obtener el valor mínimo de las coordenadas
x_min = min(x_coor_prot)
y_min = min(y_coor_prot)
z_min = min(z_coor_prot)

# se calcula la diferencia entre el valor maximo y minimo para obtener la coordenada más "lejana"
x_dif = coor_caja(abs(x_max), abs(x_min))
y_dif = coor_caja(abs(y_max), abs(y_min))
z_dif = coor_caja(abs(z_max), abs(z_min))

# se asignan los valores a las variables de las coordenadas
lx = round(x_dif * 2)
ly = round(y_dif * 2)
lz = round(z_dif * 2)

#se asignan las coordenadas de los átomos de neón
for i in range(num_atoms):
    x.append(R + (2.5 * (i % 5) * R) + (lx / 2))
    y.append(R + (2.5 * (i // 5) * R) + (ly / 2))
    z.append(R + (2.5 * (i // 5) * R) + (lz / 2))
    x_prev.append(x[i] - random.gauss(0, 0.1) * dt)
    y_prev.append(y[i] - random.gauss(0, 0.1) * dt)
    z_prev.append(z[i] - random.gauss(0, 0.1) * dt)

#se unen las listas de las coordenadas de los átomos de NE y los del PDB
x.extend(x_coor_prot)
x_prev.extend(x_coor_prot)
y.extend(y_coor_prot)
y_prev.extend(y_coor_prot)
z.extend(z_coor_prot)
z_prev.extend(z_coor_prot)

num_total = len(x)

#se inician los valores de las listas
for i in range(num_total):
    x_prox.append(0.0)
    y_prox.append(0.0)
    z_prox.append(0.0)
    fx.append(0.0)
    fy.append(0.0)
    fz.append(0.0)

# Abre el archivo
salida = open(archivo_salida, 'w')
escribe_frame(salida, x, y, z)

tiempo_empiezo = time.time()
# Loop de pasos
for paso in range(pasos):

    # La energia potencial y la energia cinetica
    pe = 0.0
    ke = 0.0
    # Las fuerzas suman de cero
    for i in range(num_total):
        fx[i] = 0.0
        fy[i] = 0.0
        fz[i] = 0.0
    # Cell decomposition
    celda = descomposicion(x, y, z)

    # Interacciones entre particulas
    for i in range(num_atoms):
        vecinos = get_vecinos(x[i], y[i], z[i])

        parejas = []
        for c in vecinos:
            if c in celda:
                for k in celda[c]:
                    if k != i:
                        parejas.append(k)

        for j in parejas:
            dx = plega(x[j] - x[i], lx)
            dy = plega(y[j] - y[i], ly)
            dz = plega(z[j] - z[i], lz)
            dist2 = dx ** 2 + dy ** 2 + dz ** 2
            if (dist2 == 0.0):
                dist2 = 0.001
            if (dist2 < cutoff ** 2):
                iz = -12.0 * lj_e * ((lj_r12 / dist2 ** 7) - (lj_r6 / dist2 ** 4))
                pe += 0.5 * lj_e * ((lj_r12 / dist2 ** 6) - (2.0 * lj_r6 / dist2 ** 3))
                fx[i] += (iz * dx)
                fy[i] += (iz * dy)
                fz[i] += (iz * dz)

    # Integracion Verlet (La segunda ley de Newton)
    for i in range(num_atoms):
        x_prox, y_prox, z_prox = calculo_Verlet(i, x, y, z, x_prev, y_prev, z_prev, fx, fy, fz, masa, dt)

    # Calcula la energia cinetica
    for i in range(num_atoms):
        velx, vely, velz = calculo_energia_cinetica(i, x_prox, x_prev, y_prox, y_prev, z_prox, z_prev, dt)

        ke += 0.5 * masa * (velx ** 2 + vely ** 2 + velz ** 2)

    # Cicla los variables
    for i in range(num_atoms):
        x_prev[i] = x[i]
        y_prev[i] = y[i]
        z_prev[i] = z[i]
        x[i] = x_prox[i]
        y[i] = y_prox[i]
        z[i] = z_prox[i]

    # Escribe el frame
    if paso % frames == 0:
        escribe_frame(salida, x, y, z)
        print('Energia: ' + str(ke + pe))

tiempo_final = time.time()
print('Tiempo: ' + str(tiempo_final - tiempo_empiezo))
salida.close()
