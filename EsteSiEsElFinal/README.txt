*Modificado desde los Script de Jeff Comer
*http://jeffcomer.us/education.html -> Códigos Python para el curso: "Programación orientada a simulación molecular y termodinámica"

README del script Saigo_no_Kibou_Ketteiteki.py

El presente README corresponde al script Saigo_no_Kibou_Ketteiteki.py.
El script fue diseñado en lenguaje Python.
Para su correcto funcionamiento es necesario Python3 o superior y 
tener instalado Biopython para obtener los datos desde el archivo PDB.

Sobre el Script:
El script permite realizar una pequeña dinamica, donde interactuan átomos de neón (NE)
contra el PDB que sea cargado. El ambiente donde se lleva a cabo las interacciones es
una caja con condiciones periodicas de borde (pbc).
El tiempo de calculo dependerá del tamaño del PDB y la cantidad de átomos que sean generados.
A continuación una tabla con referencias de tiempo y cantidad de átomos:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++ Átomos de NE +++ Átomos PDB +++ Tiempo de ejecución +++ Cantidad de Pasos +++ Frames obtenidos +++
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++      20      +++     300    +++         2 min       +++       10000       +++       1000       +++
+++      20      +++    5000    +++        30 min       +++       10000       +++       1000       +++
+++      20      +++    7300    +++        42 min       +++       10000       +++       1000       +++
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++      50      +++     300    +++         2 min       +++       10000       +++       1000       +++
+++      50      +++    5000    +++        35 min       +++       10000       +++       1000       +++
+++      50      +++    7300    +++        40 min       +++       10000       +++       1000       +++
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

El script es de código abierto. Si desea, puede cambiar las variables para que interactuaen 
otros átomos contra el PDB que desea analizar.
Al cargar el PDB que desea analizar, el script considera todo los átomos que estén en el archivo .pdb
Si necesita solamente ver interacciones con la estructura principal, se recomienda una previa limpieza del
PDB y sacar los átomos innecesarios. 
Recuerde respetar el formato PDB para que el archivo sea funcional y no presente errores en la ejecución.

Requisitos:
Se necesita python3 o superior para ejecutar este script.
Debe estar instalado Biopython en la máquina donde lo ejecutará.
Se puede ejecutar en cualquier sistema Operativo que cumpla con 
los riquisitos previamente mencionados.

Cómo ejecutar el script:

1) Abra una terminal.
2) Escriba la ruta donde se encuentra el script (Saigo_no_Kibou_Ketteiteki.py).
3) Ejecute con el siguiente comando:
    python3 Saigo_no_Kibou_Ketteiteki.py "Ruta del archivo PDB" "Nombre del archivo de salida" "Número de átomos" "Cantidad de Pasos" "Número de FRAMES"
4) Una vez finalizado el análisis, mostrará por pantalla la cantidad de tiempo que demoró en analizar su sistema.
5) Se habrá generado un archivo con el nombre que usted asignó.
6) Para visualizar el PDB generado por el script, debe ejecutar el siguiente comando por terminal: 
   vmd -f "Ruta del archivo PDB" -f "Nombre del archivo de salida"
7) Se recomienda utilizar VDW para Para visualizar los átomos de NE y NewCartoon para la estructura del PDB.

Párametros:
Para el correcto funcionamiento del script, respete el orden de los párametros así evitará fallas en la ejecución.

"Ruta del archivo PDB": La ubicación del archivo .pdb que desea analizar. Si se encuentra el .pdb en el mismo directorio
                      que el script, solo escriba el nombre del archivo junto a su extensión (.pdb). Solo se aceptan archivos
                      .pdb en el correcto formato PDB.
                      
"Nombre del archivo de salida": Escriba el nombre del archivo de salida que contendrá los frames de la dinamica realizada.
                              Recuerdo agregar la extensión .pdb junto al nombre.
                              
"Número de átomos": Párametro que regula la cantidad de átomos de Neón que interaccionarán con su sistema.

"Cantidad de Pasos": Párametro que regula la cantidad de pasos o ciclos para el análisis de interacción átomos vs estructura.

"Número de FRAMES": Párametro que regula cada cuantos pasos desea obtener un Frame de la dinamica realizada.

Ejemplo:

python3 Saigo_no_Kibou_Ketteiteki.py 6jv5.pdb output.pdb 50 10000 10

En este ejemplo se ejecuta el script. Se carga el archivo 6jv5.pdb que contiene la estructura que se desea analizar.
output.pdb es el archivo de salida del análisis realizado y que contiene los frames de la dinamica. 50 representa la cantidad
de átomos de NE que interactuaran. 10000 representa la cantidad de pasos que se realizaran y 10 es cada cuantos frames serán 
almacenados.
